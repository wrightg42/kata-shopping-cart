using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingCart;
using System;

namespace Tests
{
    [TestClass]
    public class Tests
    {
        [DataTestMethod]
        [DataRow("A", 55)]
        [DataRow("AA", 105)]
        [DataRow("ABCD", 115)]
        [DataRow("DCBCCD", 120)]
        [DataRow("DABA", 145)]
        public void TestCheckout(string items, int result)
        {
            Cart cart = new Cart();
            cart.Add(items.ToItems());
            Checkout checkout = new Checkout();
            Assert.AreEqual(result, checkout.CalculatePrice(cart));
        }

        [TestMethod]
        [DataRow("AAA", 135)]
        [DataRow("BBBB", 79)]
        public void TestOffers(string items, int result)
        {
            Cart cart = new Cart();
            cart.Add(items.ToItems());
            Checkout checkout = new Checkout();
            Assert.AreEqual(result, checkout.CalculatePrice(cart));
        }

        [DataTestMethod]
        [DataRow("AAAAADD", 180)]
        public void TestMultipleOffers(string items, int result)
        {
            Cart cart = new Cart();
            cart.Add(items.ToItems());
            Checkout checkout = new Checkout();
            Assert.AreEqual(result, checkout.CalculatePrice(cart));
        }

        [DataTestMethod]
        [DataRow("DDDDDDDDDD", 110)]
        [DataRow("DDDDDDDDDDD", 125)]
        public void TestBags(string items, int result)
        {
            Cart cart = new Cart();
            cart.Add(items.ToItems());
            Checkout checkout = new Checkout();
            Assert.AreEqual(result, checkout.CalculatePrice(cart));
        }
    
        [TestMethod]
        public void TestUnknownItem()
        {
            Cart cart = new Cart();
            cart.Add('A'.ToItem());
            cart.Add(' '.ToItem());
            cart.Add('A'.ToItem());
            Checkout checkout = new Checkout();
            Assert.AreEqual(checkout.CalculatePrice(cart), 105);
        }
    }
}
