# Kata Shopping Cart

A simple shopping cart application where a shopping cart can be filled with items, then using the checkout class the program will assess the total cost of the cart, including any discounts on items that are in the cart.

## Features

* Allows multiple different deals on items
* Reads pricing and offer details from a CSV file
* Fully tested using Unit Tests
* Seperate cart and checkout classes

## Reasoning

###### Seperate cart and checkout classes
The program was built like this to make it easier to expand the program. For example this solution could be used for a simple scan and go service, and using this class could easily be extended to make this work, then have the checkout run it's code seperatly.
###### Reading data from a CSV
The program reads data from a CSV to make it easily adjustable. If the data was hard coded this would make it hard to add deals or change prices. A CSV can be edited easily to alter the price of a product or create new offers.
###### Item class
Considering the input is a string and 'item's are characters, creating a class appears to be redundant. This is in fact here to make the project easily extendable. If product's were changed to use ID's and have more details (such as a description or image) this could be change and added to the class easily. It also works nicely as it is through the use of C# extension methods, allowing strings to be easily converted to a list of items.
###### Testing
Using a proper unit test allows me to show my solution is accurate and working accurately with ease.
