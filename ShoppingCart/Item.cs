﻿namespace ShoppingCart
{
    public class Item
    {
        public Item(char name)
        {
            Name = name;
        }

        public char Name { get; protected set; }
    }

    public static class ItemExtensions
    {
        public static Item ToItem(this char c)
        {
            return new Item(c);
        }

        public static Item[] ToItems(this string s)
        {
            Item[] items = new Item[s.Length];
            for (int i = 0; i < s.Length; ++i)
            {
                items[i] = s[i].ToItem();
            }

            return items;
        }
    }
}
