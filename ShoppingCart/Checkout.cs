﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ShoppingCart
{
    public class Checkout
    {
        public Checkout()
        {
            Prices = GetItemPrices();
        }

        public static ItemPrice[] GetItemPrices()
        {
            List<ItemPrice> itemPrices = new List<ItemPrice>();

            // read file
            using (StreamReader reader = new StreamReader("items.csv"))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] data = line.Split(','); // split csv line

                    try
                    {
                        char name = data[0][0];
                        int price = int.Parse(data[1]);

                        // get offers
                        List<Tuple<int, int>> offers = new List<Tuple<int, int>>();
                        foreach (string offer in data[2].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            // split offer into key data point
                            string[] offerData = offer.Split(new string[] { " for " }, StringSplitOptions.RemoveEmptyEntries);
                            int quantity = int.Parse(offerData[0]);
                            int offerPrice = int.Parse(offerData[1]);
                            offers.Add(new Tuple<int, int>(quantity, offerPrice));
                        }

                        itemPrices.Add(new ItemPrice(name, price, offers.ToArray()));
                    }
                    catch (FormatException e)
                    {
                        // prevent error from reading first line
                    }
                }
            }

            return itemPrices.ToArray();
        }

        private ItemPrice[] Prices { get; set; }

        public int CalculatePrice(Cart cart)
        {
            // group items in shopping cart
            var shoppingGroups = cart.Items.GroupBy(i => i.Name);

            // calculate price for each group
            int total = 0;
            foreach (var group in shoppingGroups)
            {
                // get amount of items
                int items = group.Count();
                ItemPrice price;
                try
                {
                    price = Prices.Where(p => p.Name == group.Key).First();
                }
                catch (InvalidOperationException e)
                {
                    // No items in group, invalid item in cart
                    //throw new Exception(string.Format("Invalid item in cart: {0}", group.Key));
                    continue;
                }

                // calculate prices using deals
                int dealSize = items;
                while (items > 0 && dealSize > 1)
                {
                    int dealTotal;
                    if (price.PriceRules.TryGetValue(dealSize, out dealTotal))
                    {
                        total += dealTotal;
                        items -= dealSize;
                    }
                    else
                    {
                        --dealSize;
                    }
                }

                total += items * price.Price;
            }

            int totalItems = cart.Items.Count;
            int bag = (totalItems / 5) + ((totalItems % 5 == 0) ? 0 : 1);
            int bagCost = bag * 5;

            return total + bagCost;
        }
    }
}
