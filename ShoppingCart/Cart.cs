﻿using System.Collections.Generic;

namespace ShoppingCart
{
    public class Cart
    {
        public Cart()
        {
            Items = new List<Item>();
        }

        public List<Item> Items { get; protected set; }

        public void Add(Item item)
        {
            Items.Add(item);
        }

        public void Add(Item[] items)
        {
            foreach (Item i in items)
            {
                Add(i);
            }
        }

        public void Remove(Item item)
        {
            Items.Remove(item);
        }
    }
}
