﻿using System;
using System.Collections.Generic;

namespace ShoppingCart
{
    public class ItemPrice : Item
    {
        public ItemPrice(char name, int price, params Tuple<int, int>[] priceRules)
            : base(name)
        {
            Price = price;
            PriceRules = new Dictionary<int, int>();
            foreach (Tuple<int, int> priceRule in priceRules)
            {
                PriceRules.Add(priceRule.Item1, priceRule.Item2);
            }
        }

        public int Price { get; protected set; }
        public Dictionary<int, int> PriceRules { get; protected set; }
    }
}
